<?php

namespace Emde\Notification\Sender\Service;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2016-11-22 at 01:32:40.
 */
class EmailTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Mairler config
     *
     * @var array
     */
    private $config = [
        'mailer.smtp.host' => 'example.com',
        'mailer.login' => 'notification@asd.qwe',
        'mailer.password' => 'cx332ds',
    ];
    
    /**
     * @var Email
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new Email($this->config);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        unset($this->object);
    }
    
    /**
     * @covers Emde\Notification\Sender\Service\Email::isEnabled
     * @covers Emde\Notification\Sender\Service\Email::trunOff
     * @covers Emde\Notification\Sender\Service\Email::trunOn
     */
    public function testIsEnabled()
    {
        $this->object->turnOff();
        $this->assertFalse($this->object->isEnabled());
        $this->object->turnOn();
        $this->assertTrue($this->object->isEnabled());
    }
}
