# Notification module #

## Components ##

### Notification Sites ###
They are directly responsible for sending out notifications. Each of them can be disabled at any time without causing errors. Of course, no news will be sent until the site is re-enabled.

### Registry ###
It is used to register and manage previously configured services responsible for sending notifications to individual channels (SMS, email).

### Notifiable ###
An abstraction that specifies what the subject tracked by the observers sending the notification must contain.

### Settings ###
It corresponds to storing notification channel settings in the form of a bit mask. Resolve these settings using the registry (the watchlist is returned based on the settings)

### Basket ###
Abstract basket implementation. Buying cart contents returns information about the quantity of purchased products.

### Client ###
The store customer inherits Notifiable class. After shopping (the number of items purchased is greater than 0), notify all registered and resolved observers, ie services. All notification channels in Settings are informed about client activity. If they are enabled go to send a notification. If not, finish the event handler.

## Description ##

Depending on your account settings, you may receive a sms, email notification
(none of them, one or all of them) after the occurrence of a particular event, eg placing an order.
The module will send a notification depending on these settings.
For simplicity, sms and email are delivered by external libraries.
The content of the notification is also irrelevant, it may confine itself to one simple sentence.