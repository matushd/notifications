<?php

namespace Emde\Shop;

/**
 * Basket container
 *
 * @author emde
 */
class Basket
{
    /**
     * Basket items
     *
     * @var array
     */
    private $items;
    
    public function __construct()
    {
        $this->items = [];
    }
    
    /**
     * Add item into basket
     *
     * @param mixed $element
     *
     * @return Basket
     */
    public function addItem($element) : Basket
    {
        $this->items[] = $element;
        return $this;
    }
    
    /**
     * Remove item from basket
     *
     * @param mixed $element
     *
     * @return bool
     */
    public function removeItem($element) : bool
    {
        $found = false;
        foreach ($this->items as $key => $item) {
            if ($item === $element) {
                unset($this->items[$key]);
                $found = true;
            }
        }
        return $found;
    }
    
    /**
     * Buy all items from basket
     *
     * @return int Count of purchased products
     */
    public function buy() : int
    {
        $count = count($this->items);
        $this->items = [];
        return $count;
    }
}
