<?php

namespace Emde\Shop;

use Emde\Notification\Notifiable;
use Emde\Notification\Settings;

/**
 * Shop client
 *
 * @author emde
 */
class Client extends Notifiable
{
    /**
     * Client email
     *
     * @var string
     */
    private $email;
    
    /**
     * Client phone number
     *
     * @var string
     */
    private $phone;
    
    /**
     * Client basket
     *
     * @var Basket
     */
    private $basket;
    
    /**
     * @param string $email
     * @param string $phone
     * @param Settings $settings
     */
    public function __construct(string $email, string $phone, Settings $settings)
    {
        $this->init($email, $phone, $settings);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getEmail() : string
    {
        return $this->email;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getPhone() : string
    {
        return $this->phone;
    }
    
    /**
     * Buy some stuff
     *
     * @param array $items
     * 
     * @return bool
     */
    public function buySomeStuff(array $items) : bool
    {
        foreach ($items as $element) {
            $this->basket->addItem($element);
        }
        $boughtThings = $this->basket->buy();
        if ($boughtThings) {
            $this->lastAction = sprintf('You bought %d items!', $boughtThings);
            $this->notify();
            return true;
        }
        return false;
    }
    
    /**
     * Init client data
     *
     * @param string $email
     * @param string $phone
     * @param Settings $settings
     *
     * @throws \InvalidArgumentException
     */
    private function init(string $email, string $phone, Settings $settings)
    {
        if (!$this->isValidEmail($email) || !$this->isValidPhone($phone)) {
            throw new \InvalidArgumentException(
                sprintf('Invalid email or phone! email: %s phone: %s', $email, $phone)
            );
        }
        $this->email = $email;
        $this->phone = $phone;
        $this->settings = $settings;
        $this->basket = new Basket;
    }
    
    /**
     * Validate email
     *
     * @param string $email
     *
     * @return bool
     */
    private function isValidEmail(string $email) : bool
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }
    
    /**
     * Validate phone
     *
     * @param string $phone
     *
     * @return bool
     */
    private function isValidPhone(string $phone) : bool
    {
        return preg_match('/\+?([0-9]{3})([ .-]?)([0-9]{3})\2([0-9]{4})/', $phone) !== false;
    }
}
