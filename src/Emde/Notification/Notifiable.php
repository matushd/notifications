<?php

namespace Emde\Notification;

use Emde\Notification\Sender\Registry;

/**
 * Notifiable element
 *
 * @author emde
 */
abstract class Notifiable
{
    /**
     * Get email address
     *
     * @return string
     */
    abstract public function getEmail() : string;
    
    /**
     * Get phone number
     *
     * @return string
     */
    abstract public function getPhone() : string;
    
    /**
     * Notification channel settings
     *
     * @var \Emde\Notification\Settings
     */
    protected $settings;
    
    /**
     * Last action description
     *
     * @var string
     */
    protected $lastAction;
    
    /**
     * Get last action description
     *
     * @return string
     */
    public function getLastAction() : string
    {
        return $this->lastAction;
    }
    
    /**
     * Notify an observer
     */
    public function notify()
    {
        $observers = Registry::getInstance()->resolve($this->settings);
        foreach ($observers as $observer) {
            $observer->update($this);
        }
    }
}
