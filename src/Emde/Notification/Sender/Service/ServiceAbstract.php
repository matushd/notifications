<?php

namespace Emde\Notification\Sender\Service;

use Emde\Notification\Notifiable;

/**
 * Notification sender interface
 *
 * @author emde
 */
abstract class ServiceAbstract
{
    /**
     * Service api client instance
     *
     * @var mixed
     */
    protected $api;
    
    /**
     * Is service enabled
     *
     * @var boolean
     */
    private $enabled;
    
    /**
     * Send notification message to subject
     *
     * @param Notifiable $subject
     *
     * @return bool
     */
    abstract protected function send(Notifiable $subject): bool;
    
    /**
     * Init service
     *
     * @param array $config
     *
     * @throws \InvalidArgumentException
     */
    abstract protected function init(array $config);
    
    /**
     * @param array $config
     * @param bool $enabled
     */
    public function __construct(array $config, bool $enabled = true)
    {
        $this->init($config);
        $this->enabled = $enabled;
    }
    
    /**
     * Handle subject changes if service is enabled
     *
     * @param Notifiable $subject
     *
     * @return bool
     */
    final public function update(Notifiable $subject): bool
    {
        if (!$this->isEnabled()) {
            return false;
        }
        return $this->send($subject);
    }
    
    /**
     * Is service enabled
     *
     * @return bool
     */
    public function isEnabled() : bool
    {
        return $this->enabled;
    }
    
    /**
     * Turn off the service
     */
    public function turnOff()
    {
        $this->enabled = false;
    }
    
    /**
     * Turn on the service
     */
    public function turnOn()
    {
        $this->enabled = true;
    }
}
