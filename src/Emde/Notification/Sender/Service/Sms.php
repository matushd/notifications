<?php

namespace Emde\Notification\Sender\Service;

use SMSApi\Api\SmsFactory;
use SMSApi\Client;
use Emde\Notification\Notifiable;
use Emde\Notification\Sender\Exception\SendNotificationException;

/**
 * Sms sender service
 *
 * @author emde
 */
class Sms extends ServiceAbstract
{
    /**
     * Config properties
     */
    const CONFIG_LOGIN = 'smsapi.login';
    const CONFIG_PASSWORD = 'smsapi.password';

    /**
     * {@inheritdoc}
     */
    protected function send(Notifiable $subject): bool
    {
        try {
            $actionSend = $this->api->actionSend();
            $actionSend->setTo($subject->getPhone());
            $actionSend->setText($subject->getLastAction());
            $actionSend->setSender('Johny Bravo');
            
            $response = $actionSend->execute()->getList();
            if ($response->getStatus() === 'SENT') {
                return true;
            }
        } catch (\Exception $e) {
            throw new SendNotificationException($e->getMessage(), null, $e);
        }
        throw new SendNotificationException(
            sprintf('Invalid sms response status: %s for %s', $response->getStatus(), $subject->getPhone())
        );
    }
    
    /**
     * Init sms api
     *
     * {@inheritdoc}
     */
    protected function init(array $config)
    {
        if (!isset($config[static::CONFIG_LOGIN]) || !isset($config[static::CONFIG_PASSWORD])) {
            throw new \InvalidArgumentException('Invalid smsapi config!');
        }
        $client = new Client(static::CONFIG_LOGIN);
        $client->setPasswordHash(static::CONFIG_PASSWORD);
        $this->api = new SmsFactory();
        $this->api->setClient($client);
    }
}
