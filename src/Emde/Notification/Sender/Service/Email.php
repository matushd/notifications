<?php

namespace Emde\Notification\Sender\Service;

use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use Emde\Notification\Notifiable;
use Emde\Notification\Sender\Exception\SendNotificationException;

/**
 * Email sender service
 *
 * @author emde
 */
class Email extends ServiceAbstract
{
    /**
     * Sender email
     *
     * @var string
     */
    private $sender;
    
    /**
     * Config properties
     */
    const CONFIG_LOGIN = 'mailer.login';
    const CONFIG_PASSWORD = 'mailer.password';
    const CONFIG_SMTP_HOST = 'mailer.smtp.host';
    const CONFIG_SMTP_PORT = 'mailer.smtp.port';
    const CONFIG_SMTP_ENCRYPTION = 'mailer.smtp.encryption';

    /**
     * {@inheritdoc}
     */
    protected function send(Notifiable $subject): bool
    {
        $message = Swift_Message::newInstance()
            ->setSubject('Notification')
            ->setFrom($this->sender)
            ->setTo($subject->getEmail())
            ->setBody($subject->getLastAction());
        if (!$this->api->send($message)) {
            throw new SendNotificationException(
                sprintf('Can\'t send notification to %s!', $notification->getRecipient())
            );
        }
        return true;
    }
    
    /**
     * Init mailer
     *
     * {@inheritdoc}
     */
    protected function init(array $config)
    {
        if (!isset($config[static::CONFIG_LOGIN]) || !isset($config[static::CONFIG_PASSWORD])
            || !isset($config[static::CONFIG_SMTP_HOST])) {
            throw new \InvalidArgumentException('Invalid mailer config!');
        }
        $transport = Swift_SmtpTransport::newInstance(
            $config[static::CONFIG_SMTP_HOST],
            $config[static::CONFIG_SMTP_PORT] ?? 25,
            $config[static::CONFIG_SMTP_ENCRYPTION] ?? 'ssl'
        );
        $transport->setUsername($config[static::CONFIG_LOGIN]);
        $transport->setPassword($config[static::CONFIG_PASSWORD]);
        $this->api = Swift_Mailer::newInstance($transport);
        $this->sender = $config[static::CONFIG_LOGIN];
    }
}
