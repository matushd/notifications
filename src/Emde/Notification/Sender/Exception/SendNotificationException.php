<?php

namespace Emde\Notification\Sender\Exception;

use Exception;

/**
 * Send notification exception
 *
 * @author emde
 */
class SendNotificationException extends Exception
{
}
