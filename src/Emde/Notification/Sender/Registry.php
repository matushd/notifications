<?php

namespace Emde\Notification\Sender;

use Emde\Notification\Settings;

/**
 *
 * @author emde
 */
class Registry
{
    /**
     * Service types
     */
    const SERVICE_SMS = 1;
    const SERVICE_EMAIL = 2;
    
    /**
     * Service map
     *
     * @var array
     */
    private static $serviceMap = [
        self::SERVICE_SMS => 'Emde\Notification\Sender\Service\Sms',
        self::SERVICE_EMAIL => 'Emde\Notification\Sender\Service\Email',
    ];
    
    /**
     * Registry instance
     *
     * @var \Emde\Notification\Sender\Registry
     */
    private static $instance = null;
    
    /**
     * Registered services
     *
     * @var \Emde\Notification\Sender\ServServiceAbstract[]
     */
    private $services;
    
    /**
     * Is service available
     *
     * @param int $service
     *
     * @return bool
     */
    public static function serviceExists(int $service) : bool
    {
        return isset(static::$serviceMap[$service]);
    }
    
    /**
     * Get Registry instance
     *
     * @return Registry
     */
    public static function getInstance()
    {
        if (!isset(static::$instance)) {
            static::$instance = new static();
        }
        return static::$instance;
    }
    
    /**
     * Create and register service if not exists
     *
     * @param int $service
     * @param array $config Service config
     */
    public function register(int $service, array $config)
    {
        if (static::serviceExists($service) && !isset($this->services[$service])) {
            $this->services[$service] = new static::$serviceMap[$service]($config);
        }
    }
    
    /**
     * Resolve notification settings to services
     *
     * @param Settings $settings
     *
     * @return \Emde\Notification\Sender\Service\ServiceAbstract[]
     */
    public function resolve(Settings $settings)
    {
        $resolved = [];
        foreach ($this->services as $type => $service) {
            if ($settings->isChannelEnabled($type)) {
                $resolved[$type] = $service;
            }
        }
        return $resolved;
    }
    
    /**
     * Disable one of registered service
     *
     * @param int $service
     */
    public function disableService(int $service)
    {
        $this->validate($service);
        $this->services[$service]->turnOff();
    }
    
    /**
     * Enable one of registered service
     *
     * @param int $service
     */
    public function enableService(int $service)
    {
        $this->validate($service);
        $this->services[$service]->turnOn();
    }
    
    /**
     * Disable new instance possibility
     */
    private function __construct()
    {
        $this->services = [];
    }
    
    /**
     * Disable clone
     */
    private function __clone()
    {
    }

    /**
     * Disable deserialization
     */
    private function __wakeup()
    {
    }
    
    /**
     * Check whether service is registered
     *
     * @param int $service
     *
     * @throws \InvalidArgumentException
     */
    private function validate(int $service)
    {
        if (!isset($this->services[$service])) {
            throw new \InvalidArgumentException(sprintf('Service %d is not registered!', $service));
        }
    }
}
