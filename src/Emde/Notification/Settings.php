<?php

namespace Emde\Notification;

use Emde\Notification\Sender\Registry;

/**
 * Notification settings container
 *
 * @author emde
 */
class Settings
{
    /**
     * Settings bit flag
     *
     * @var int
     */
    private $flag;
    
    public function __construct()
    {
        $this->flag = 0;
    }
    
    /**
     * Enable notification channel
     *
     * @param int $type
     *
     * @return \Emde\Notification\Settings
     */
    public function enableChannel(int $type)
    {
        $this->validate($type);
        $this->flag |= $type;
        return $this;
    }
    
    /**
     * Disable notification channel
     *
     * @param int $type
     *
     * @return \Emde\Notification\Settings
     */
    public function disableChannel(int $type)
    {
        $this->validate($type);
        $this->flag &= (~$type);
        return $this;
    }
    
    /**
     * Check whether the notification channel is enabled
     *
     * @param int $type
     *
     * @return boolean
     */
    public function isChannelEnabled(int $type)
    {
        $this->validate($type);
        return (bool)($this->flag & $type);
    }
    
    /**
     * Check is type valid
     *
     * @param int $type
     *
     * @throws \InvalidArgumentException
     */
    private function validate(int $type)
    {
        if (!Registry::serviceExists($type)) {
            throw new \InvalidArgumentException(sprintf('Type %d is invalid!', $type));
        }
    }
}
